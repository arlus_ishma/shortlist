from django.conf.urls.defaults import patterns, include, url
#from django.views.generic.simple import direct_to_template
from main import views
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'shortlist.views.home', name='home'),
    # url(r'^shortlist/', include('shortlist.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration_email.backends.default.urls')),
    #url(r'^$', direct_to_template,
    # { 'template': 'base.html' }, 'home_page'),
    url(r'^$', views.homepage, name='index'),
    url(r'^admin_panel/$', views.admin_panel, name='admin-panel'),
    url(r'^job/(?P<job_id>\w+)/$', views.job_status, name='job_status'),
    url(r'^applicants/(?P<job_id>\w+)/$',views.refresh_applicants, name='refresh_applicants'),
    url(r'^best_match/(?P<job_id>\w+)/$',views.best_match, name='best_match'),
    url(r'^skill_filter/(?P<job_id>\w+)/$',views.skill_filter, name='skill_filter'),
    url(r'^experience_filter/(?P<job_id>\w+)/$',views.xperience_filter, name='xperience_filter'),
    url(r'^academic_filter/(?P<job_id>\w+)/$',views.academic_filter, name='academic_filter'),
    url(r'^post_job/$',views.post_job, name='post_job'),
    url(r'^apply/(?P<job_id>\w+)/$',views.apply, name='apply'),
    url(r'^(?P<job_id>\w+)/(?P<candidate_id>\w+)/filter/$',views.filter, name='filter'),
    url(r'^(?P<job_id>\w+)/(?P<candidate_id>\w+)/shortlist/$',views.shortlist, name='shortlist'),
    url(r'^(?P<job_id>\w+)/shortlisted/$',views.view_shortlisted, name='view_shortlisted'),
    #url(r'^(?P<shortlist_id>\w+)/$',views.remove_shortlisted, name='remove'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}))

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))