Django==1.3
PyYAML==3.10
South==0.7.6
argparse==1.2.1
distribute==0.6.24
django-autoload==0.01
django-dbindexer==0.3
django-mongodb-engine==0.4.0
django-registration==0.8
-e git://github.com/bitmazk/django-registration-email@d7db3dacb812f759bae68049460cbdf5344178fc#egg=django_registration_email-dev
djangoappengine==1.0
djangotoolbox==0.9.2
-e git://github.com/seatgeek/fuzzywuzzy.git@e91af7742423831ca0b334638a0e4496737a96db#egg=fuzzywuzzy-dev
jellyfish==0.2.0
nltk==2.0.4
pymongo==2.5
wsgiref==0.1.2
