import os
from django.db import models
from djangotoolbox.fields import EmbeddedModelField, ListField
import hashlib
import datetime
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.conf import settings
# Create your models here.

def get_file_path(instance, filename):
    return os.path.join('files', hashlib.md5(str(datetime.datetime.now)).hexdigest())


class Job(models.Model):
    owner = models.ForeignKey(User, unique=False)
    age_limit = models.IntegerField()
    name = models.CharField(max_length=200)
    description = models.TextField()
    is_active = models.BooleanField(default=True)
    is_filtered = models.BooleanField(default=False)
    skillset = models.TextField(max_length=255)
    years_of_experience = models.IntegerField()
    academics = models.CharField(max_length=255)
    posted_on = models.DateTimeField(auto_now_add=True)
    job_url = models.CharField(max_length=255, blank=True)
    last_application_date = models.DateField(blank=True)
    applicant_limit = models.IntegerField()
    applicants = models.IntegerField(default=0)

    class Meta:
        db_table = 'Job'
        ordering = ['-posted_on']
        verbose_name_plural = 'Jobs'

    def save(self, force_insert=False, force_update=False):
        self.applicants+=1
        super(Job, self).save(force_insert, force_update)

    def __unicode__(self):
        return u'%s' % self.id
    @models.permalink
    def get_absolute_url(self):
        return ('jobs_job', (), { 'job_id': self.id })

import os
def notify(sender, instance, **kwargs):
    if instance.job_url == '':
        post_save.disconnect(notify, sender=Job)
        url = "http://" + settings.SITE + "/apply/" + instance.id + "/"
        instance.job_url = url
        instance.save()

post_save.connect(notify, sender=Job, dispatch_uid="main.models")

class Candidate(models.Model):
    job = models.ForeignKey('Job', unique=False)
    name = models.CharField(max_length=200)
    email = models.EmailField()
    cv = models.FileField(upload_to=get_file_path)
    years_of_experience = models.IntegerField()
    skillset = models.CharField(max_length=255)
    academics = models.CharField(max_length=255)
    applied_on = models.DateTimeField(auto_now_add=True)
    filtered_out = models.BooleanField(default=False)
    shortlisted = models.BooleanField(default=False)
    number = models.CharField(max_length=50)
    cover_letter = models.TextField(blank=True)
    class Meta:
        db_table = 'Candidate'
        ordering = ['-applied_on']
        verbose_name_plural = 'Candidates'
    def __unicode__(self):
        return self.name
    @models.permalink
    def get_absolute_url(self):
        return ('jobs_candidate', (), { 'candidate_name': self.email })

class Shortlist(models.Model):
    job = models.ForeignKey('Job', unique=False)
    email = models.EmailField()
    can_id = models.CharField(max_length=250)
    name = models.CharField(max_length=200)
    shortlisted_on = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = 'Shortlist'
        ordering = ['-shortlisted_on']
        verbose_name_plural = 'Shortlists'
    def __unicode__(self):
        return self.name
    @models.permalink
    def get_absolute_url(self):
        return ('jobs_shortlist', (), { 'shortlist_name': self.email })
