from django.contrib import admin
from django.contrib.admin import site, ModelAdmin
from main.models import Job, Candidate, Shortlist

def candidates(instance):
    return ', '.join(instance.candidates)

def shortlists(instance):
    return ', '.join(instance.shortlists)

class CandidateAdmin(ModelAdmin):
    list_display = [candidates]

class ShortlistAdmin(ModelAdmin):
    list_display = [shortlists]

admin.site.register(Job)
admin.site.register(Candidate)
admin.site.register(Shortlist)