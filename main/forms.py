from django import forms
from django.conf import settings
from django.contrib.admin import widgets

class JobForm(forms.Form):
    title = forms.CharField()
    description = forms.CharField(widget=forms.Textarea)
    applicant_limit = forms.IntegerField()
    age_limit = forms.IntegerField()
    years_of_experience = forms.IntegerField()
    last_application_date = forms.DateField(widget=widgets.AdminDateWidget)
    skillset = forms.CharField()
    academics = forms.CharField()

    def __init__(self, *args, **kwargs):
        super(JobForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['class'] = 'input-xxlarge'
        self.fields['title'].widget.attrs['placeholder'] = 'Job title'
        self.fields['description'].widget.attrs['class'] = 'input-xxlarge'
        self.fields['description'].widget.attrs['placeholder'] = 'Description of the job position. Be as detailed as possible'
        self.fields['applicant_limit'].widget.attrs['class'] = 'input-small'
        self.fields['age_limit'].widget.attrs['class'] = 'input-small'
        self.fields['years_of_experience'].widget.attrs['class'] = 'input-small'
        self.fields['last_application_date'].widget.attrs['class'] = 'input-large datepicker'
        self.fields['skillset'].widget.attrs['class'] = 'input-xlarge tagManager'
        self.fields['skillset'].widget.attrs['placeholder'] = 'Skills required. Separate using commas.'
        self.fields['academics'].widget.attrs['class'] = 'input-xxlarge tagManager'
        self.fields['academics'].widget.attrs['placeholder'] = 'Academics required. Separate using commas.'



class ApplicationForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    cv = forms.FileField()
    years_of_experience = forms.IntegerField()
    skillset = forms.CharField()
    academics = forms.CharField()
    number = forms.CharField()
    cover_letter = forms.CharField(widget = forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(ApplicationForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'input-xxlarge'
        self.fields['name'].widget.attrs['placeholder'] = 'Your name'
        self.fields['cover_letter'].widget.attrs['class'] = 'input-xxlarge'
        self.fields['cover_letter'].widget.attrs['placeholder'] = 'Cover letter. Be as detailed as possible'
        self.fields['cv'].widget.attrs['class'] = 'input-xlarge'
        self.fields['academics'].widget.attrs['class'] = 'input-xxlarge tagManager'
        self.fields['skillset'].widget.attrs['class'] = 'input-xlarge tagManager'
        self.fields['skillset'].widget.attrs['placeholder'] = 'Your skills. Separate using commas.'
        self.fields['years_of_experience'].widget.attrs['class'] = 'input-small'
        self.fields['number'].widget.attrs['class'] = 'input-xlarge'
        self.fields['academics'].widget.attrs['placeholder'] = 'Your academic qualifications. Separate using commas.'



