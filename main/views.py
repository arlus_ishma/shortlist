# Create your views here.
from django.template import RequestContext
from django.shortcuts import render_to_response
from main.models import Job, Candidate, Shortlist
from django.http import HttpResponse, HttpResponseRedirect
from django.core import serializers
import nltk
from nltk.corpus import stopwords
from django.utils import simplejson
import jellyfish
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from forms import JobForm, ApplicationForm
import os
from django.conf import settings


def get_user(email):
    try:
        return User.objects.get(email=email.lower())
    except User.DoesNotExist:
        return None

def homepage(request):
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        username = get_user(email)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect("/admin_panel/")
            else:
                # Return a 'disabled account' error message
                pass
        else:
            return render_to_response("home.html", context_instance=RequestContext(request))
    else:
        return render_to_response("home.html", context_instance=RequestContext(request))

@login_required
def admin_panel(request):
    my_jobs = Job.objects.filter(owner=request.user.id)
    return render_to_response("main/admin-panel.html", locals(), context_instance=RequestContext(request))

@login_required
def job_status(request, job_id):
    details = Job.objects.get(id=job_id)
    candidates = Candidate.objects.filter(job=job_id, filtered_out=False)
    shortlists = Shortlist.objects.filter(job=job_id)
    return render_to_response("main/details.html", locals(), context_instance=RequestContext(request))

@login_required
def refresh_applicants(request, job_id):
    applicants = list(Candidate.objects.filter(job=job_id, filtered_out=False).values('name'))
    if request.is_ajax():
        data = simplejson.dumps(applicants)
        return HttpResponse(data, mimetype="application/json")
    else:
        applicants = list(Candidate.objects.filter(job=job_id, filtered_out=False).values('name'))

        data = simplejson.dumps(applicants)
        return HttpResponse(data, mimetype="application/json")

@login_required
def skill_filter(request, job_id):
    job_skills = Job.objects.get(pk=job_id).skillset
#    if request.method == "POST" and request.is_ajax:
#        filtered_out = 0
#        for candidate in Candidate.objects.filter(job=job_id):
#            applicant_skills = candidate.skillset
#            if set(job_skills).issubset(set(applicant_skills)):
#                pass
#            else:
#                candidate.is_filtered = True
#                candidate.save()
#                filtered_out += 1
#        return HttpResponse(filtered_out, mimetype="application/json")
    if request.method == "POST":
        import sys
        filtered_out = 0
        for candidate in Candidate.objects.filter(job=job_id, filtered_out=False):
            applicant_skills = candidate.skillset
            if set(job_skills).issubset(set(applicant_skills)):
                pass
            else:
                candidate.filtered_out = True
                candidate.save()
                filtered_out += 1
        return HttpResponseRedirect('/job/' + job_id + '/')

@login_required
def academic_filter(request, job_id):
#        filtered_out = 0
    job_academics = Job.objects.get(pk=job_id).academics
#        for candidate in Candidate.objects.filter(job=job_id):
#            applicant_academics = candidate.academics
#            if set(job_academics).issubset(set(applicant_academics)):
#                pass
#            else:
#                candidate.is_filtered = True
#                candidate.save()
#                filtered_out += 1
#        return HttpResponse(filtered_out, mimetype="application/json")
    if request.method == "POST":
        filtered_out = 0
        for candidate in Candidate.objects.filter(job=job_id):
            applicant_academics = candidate.academics
            if set(job_academics).issubset(set(applicant_academics)):
                continue
            else:
                candidate.filtered_out = True
                candidate.save()
                filtered_out += 1
        return HttpResponseRedirect('/job/' + job_id + '/')

def process(text):
    tokens = nltk.word_tokenize(text)
    stopwords = nltk.corpus.stopwords.words('english')
    content = [w for w in text if w.lower() not in stopwords]
    names = nltk.corpus.names
    male_names = names.words('male.txt')
    female_names = names.words('female.txt')
    content2 = [i for i in content if i.lower() not in male_names]
    content3 = [i for i in content2 if i.lower() not in female_names]
    return content3

def compare(text1, text2):
    ratio = jellyfish.jaro_distance(str(set(text1)), str(set(text2)))
    return ratio

@login_required
def best_match(request, job_id):
    ratios = {}
    description = process(Job.objects.get(pk=job_id).description)
#    if request.is_ajax:
#        for candidate in Candidate.objects.filter(job=job_id):
#            text2 = process(candidate.cover_letter)
#            record = compare(description, text2)
#            ratios[candidate.id] = record
#        data = simplejson.dumps(ratios)
#        best = max(ratios.iterkeys(), key=lambda k: ratios[k])
#        return HttpResponse(best, mimetype="application/json")
#    else:
    for candidate in Candidate.objects.filter(job=job_id):
        text2 = process(candidate.cover_letter)
        record = compare(description, text2)
        ratios[candidate.id] = record
    data = simplejson.dumps(ratios)
    best = max(ratios.iterkeys(), key=lambda k: ratios[k])
    output = Candidate.objects.get(id=best)
    return render_to_response("main/best_match.html", locals(), context_instance=RequestContext(request))


def post_job(request):
    if request.method == "POST":
        form = JobForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            saves = Job(owner = request.user, age_limit = data['age_limit'], name = data['title'],
                description = data['description'], skillset = data['skillset'], years_of_experience = data['years_of_experience'],
                academics = data['academics'], applicant_limit = data['applicant_limit'],
                last_application_date = data['last_application_date'])
            saves.save()
            my_jobs = Job.objects.filter(owner=request.user.id)
            return render_to_response("main/admin-panel.html", locals(), context_instance=RequestContext(request))
    else:
        form = JobForm()
    return render_to_response("main/post_job.html", locals(), context_instance=RequestContext(request))


def apply(request, job_id):
    if request.method == "POST":
        form = ApplicationForm(request.POST, request.FILES)
        current_job = Job.objects.get(id=job_id)
        if form.is_valid():
            data = form.cleaned_data
            saves = Candidate(job=current_job, name = data['name'], email = data['email'], years_of_experience = data['years_of_experience'],
                skillset = data['skillset'], academics = data['academics'], number = data['number'], cover_lettr = data['cover_letter'], cv = request.FILES['cv'])
            saves.save()
            return render_to_response("main/application_sent.html", context_instance = RequestContext(request))
    else:
        form = ApplicationForm()
        job = Job.objects.get(id=job_id)
        current_job = Job.objects.get(id=job_id)
    return render_to_response("main/apply.html", locals(), context_instance=RequestContext(request))

def filter(request, job_id, candidate_id):
    if request.method == "POST":
        current = Candidate.objects.get(id=candidate_id, job=job_id)
        current.filtered_out = True
        current.save()
        return HttpResponseRedirect('/job/' + job_id + '/')

def shortlist(request, job_id, candidate_id):
    candidate = Candidate.objects.get(id=candidate_id)
    job = Job.objects.get(id=job_id)
    if request.method == "POST":
        try:
            fetch = Shortlist.objects.get(job=job, can_id=candidate_id) == ''
            return HttpResponseRedirect('/job/' + job_id + '/')
        except Shortlist.DoesNotExist:
            saves = Shortlist(job=job, email=candidate.email, name = candidate.name, can_id = candidate_id)
            saves.save()
            can = Candidate.objects.get(id=candidate_id)
            can.shortlisted = True
            can.save()
            return HttpResponseRedirect('/job/' + job_id + '/')

@login_required
def xperience_filter(request, job_id):
    job_xperience = Job.objects.get(pk=job_id).years_of_experience
    #    if request.method == "POST" and request.is_ajax:
    #        filtered_out = 0
    #        for candidate in Candidate.objects.filter(job=job_id):
    #            applicant_skills = candidate.skillset
    #            if set(job_skills).issubset(set(applicant_skills)):
    #                pass
    #            else:
    #                candidate.is_filtered = True
    #                candidate.save()
    #                filtered_out += 1
    #        return HttpResponse(filtered_out, mimetype="application/json")
    if request.method == "POST":
        import sys
        filtered_out = 0
        for candidate in Candidate.objects.filter(job=job_id, filtered_out=False):
            applicant_xperience = candidate.years_of_experience
            if int(job_xperience) <= int(applicant_xperience):
                pass
            else:
                candidate.filtered_out = True
                candidate.save()
                filtered_out += 1
        return HttpResponseRedirect('/job/' + job_id + '/')

@login_required
def view_shortlisted(request, job_id):
    shortlisted = Shortlist.objects.all()
    #if request.method == 'POST':
        
    return render_to_response("main/shortlisted.html", locals(), context_instance=RequestContext(request))






